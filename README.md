# Individual project 2

This project is a simple web application built with Rust and the Actix web framework. It provides a user-friendly interface for entering a list of numbers and calculates their sum.

# Features
* REST API Endpoint: Processes JSON requests to calculate the sum of numbers.
* Interactive Web Interface: Users can input a list of numbers to calculate their sum directly from their browser.
## Please refer to the demo video in the repo: demo.mov

## Author

Yanbo Guan
## Project Structure
The project consists of a main Rust application that sets up an Actix web server with two endpoints:

1. A GET endpoint (/) that serves an HTML page. This page includes a form where users can input a list of numbers.
2. A POST endpoint (/sum) that accepts JSON containing a list of numbers, calculates their sum, and returns the result in JSON format.

## Key Components
1. SumRequest: A struct that defines the expected format of JSON requests containing the list of numbers to sum.
2. SumResponse: A struct that defines the format of JSON responses that provide the sum of the numbers.
3. sum_numbers: An asynchronous function that handles POST requests to the /sum endpoint, calculates the sum of the provided numbers, and returns the result.
4. index: An asynchronous function that serves the HTML for the web interface, enabling users to input their list of numbers and view the sum.
## Usage
After starting the web service, navigate to http://localhost:8080 in your web browser. Enter a list of numbers separated by commas in the input field (e.g., 1,2,3) and click the Calculate the sum button. The sum of the numbers will be displayed on the page.


## Configuration and Initialization:
### Install Docker (I am working on mac OS) [link](https://www.docker.com/get-started/)

1. Create a new Rust Project:
```cargo new rust_microservice```
2. Navigate to the project directory:
```cd rust_microservic```
3. Add dependencies to Cargo.toml file.
4. Develop application code in the src/main.rs file
5. Build and run: ```cargo run```
6. Containerize the Rust Actix Web App: 
![](docker_file.png)
7. Run the following command in the same directory as your Dockerfile:
```docker build -t rust_microservic .```
8. Run the Container locally:
```docker run -p 8080:8080 rust_microservic```
9. Access the webservice at http://localhost:8080
![](1.png)
![](2.png)