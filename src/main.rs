use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct SumRequest {
    numbers: Vec<i32>,
}

#[derive(Serialize)]
struct SumResponse {
    sum: i32,
}

// Endpoint to calculate the sum of numbers from a JSON request
async fn sum_numbers(item: web::Json<SumRequest>) -> impl Responder {
    let sum: i32 = item.numbers.iter().sum();
    web::Json(SumResponse { sum })
}

// Endpoint to serve the HTML page
async fn index() -> impl Responder {
    let html = r#"
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Sum Calculator</title>
        </head>
        <body>
            <h2>Hi, this is a demo from Yanbo, please enter a list:</h2>
            <input type="text" id="numberList" placeholder="1,2,3(for example)">
            <button onclick="calculateSum()">Calculate the sum</button>
            <p id="result"></p>

            <script>
                async function calculateSum() {
                    const numbers = document.getElementById('numberList').value.split(',').map(Number);
                    const response = await fetch('/sum', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({ numbers }),
                    });

                    if (response.ok) {
                        const data = await response.json();
                        document.getElementById('result').textContent = 'Here is the sum of the list: ' + data.sum;
                    } else {
                        document.getElementById('result').textContent = 'Error calculating sum';
                    }
                }
            </script>
        </body>
        </html>
    "#;
    HttpResponse::Ok().content_type("text/html").body(html)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(index))
            .route("/sum", web::post().to(sum_numbers))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
